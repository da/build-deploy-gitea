#!/bin/bash -ex

function random_password () {
	# $1: amount of letters
	cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold --w $1 | tr '[:upper:]' '[:lower:]' | head -n 1
}

HOSTNAME_FQDN=$1

TMP=`mktemp -d /tmp/XXXXXXXXX`
cp -r etc ${TMP}
grep -rl '${HOSTNAME_FQDN}' ${TMP} | xargs sed -i "s/\${HOSTNAME_FQDN}/${HOSTNAME_FQDN}/g"


if [[ ${HOSTNAME_FQDN} == *test.* ]] ; then
	BANNER="
		<div style='background: red; color: yellow; text-align: center; font-weight: 900;'>
			WARNING: THIS IS A TEST INSTANCE. DATA CAN VANISH AT ANY TIME.
		</div>"
	echo "${BANNER}" > ${TMP}/etc/gitea/templates/custom/body_outer_pre.tmpl
	echo "Disallow: /" > ${TMP}/etc/gitea/robots.txt
fi

rsync -av -e ssh --chown=root:root ${TMP}/etc root@${HOSTNAME_FQDN}:/
rsync -av -e ssh --delete --chown=git:git ${TMP}/etc/gitea root@${HOSTNAME_FQDN}:/etc/
rsync -av -e ssh --delete --chown=git:git ${TMP}/etc/gitea/public root@${HOSTNAME_FQDN}:/etc/gitea/
rm -rf ${TMP}

# continue with deployment
	ssh root@${HOSTNAME_FQDN} chown git:git /etc/gitea -R
	rsync -av -e ssh --delete --chown=www-data:www-data var/www/pages root@${HOSTNAME_FQDN}:/var/www/
	# does not work anyway
	# -ssh root@${HOSTNAME_FQDN} "sudo -u git -g git /data/git/bin/gitea manager flush-queues"

# stop services and put new Gitea in place
	ssh root@${HOSTNAME_FQDN} "systemctl daemon-reload && systemctl stop gitea && mv /data/git/bin/gitea.new /data/git/bin/gitea"

# generate redis password if not set and write to configs
	: ${REDIS_PWD:=$(random_password 64)}
	ssh root@${HOSTNAME_FQDN} "sed -i 's/requirepass .*/requirepass ${REDIS_PWD}/g' /etc/redis/redis.conf"
	ssh root@${HOSTNAME_FQDN} "sed -i 's/\${REDIS_PWD}/${REDIS_PWD}/g' /etc/gitea/conf/app.ini"

# generate mariadb password if not set and write to configs on remote
	: ${MARIADB_PWD:=$(random_password 64)}
	ssh root@${HOSTNAME_FQDN} "mysql -e \"SET PASSWORD FOR 'gitea'@localhost = PASSWORD(\\\"${MARIADB_PWD}\\\");\""
	ssh root@${HOSTNAME_FQDN} "sed -i 's/\${MARIADB_PWD}/${MARIADB_PWD}/g' /etc/gitea/conf/app.ini"

# bring all services back online and apply changes
	ssh root@${HOSTNAME_FQDN} "systemctl restart redis && systemctl start gitea && systemctl enable gitea && systemctl status gitea"


